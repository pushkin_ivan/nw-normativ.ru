#!/bin/bash

echo "starting pnd f registry copier"

if [ -z "$1" ]; then
	echo "no argument supplied"
	exit
fi

if [ ! -d "$1" ]; then
	echo "passed directory does not exist"
	exit
fi

TARGET_DIR=$1
TMP_DIR="/tmp/pnd_f"
SCRIPT_LOCATION=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
URL="http://fcao.ru/metodiki-kkha/2-uncategorised/114-perechni-metodik-vklyuchennykh-v-reestr-pnd-f.html"

echo "checking tmp directory"

if [ -d $TMP_DIR ]; then
	rm $TMP_DIR -r
fi

mkdir $TMP_DIR

echo "loading registry files from $URL"

$SCRIPT_LOCATION/loader --url=$URL --dir=$TMP_DIR

echo "extracting text and generation csv files"

for docFile in $TMP_DIR/*.doc; do
	echo "extracting text from $docFile"
	txtFile="$docFile.txt"
	antiword $docFile > $txtFile
	rm $docFile

	csvFile="$txtFile.csv"
	echo "generation csv file from $txtFile"
	$SCRIPT_LOCATION/conv_csv --file=$txtFile --csv=$csvFile
	rm $txtFile

	mv $csvFile $TARGET_DIR
done

rm $TMP_DIR -r
