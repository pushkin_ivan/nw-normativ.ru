#!/bin/bash

echo "starting pnd f registry copier"

if [ -z "$1" ]; then
	echo "no argument supplied"
	exit
fi

if [ ! -d "$1" ]; then
	echo "passed directory does not exist"
	exit
fi

TARGET_DIR=$1
TMP_DIR=$1
SCRIPT_LOCATION=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "checking tmp directory"

# if [ -d $TMP_DIR ]; then
#	rm $TMP_DIR -r
# fi

# mkdir $TMP_DIR

echo "loading registry files from $URL"

echo "extracting text and generation csv files"

for docFile in $TMP_DIR/*.doc; do
	echo "extracting text from $docFile"
	txtFile="$docFile.txt"
	antiword $docFile > $txtFile
	#rm $docFile

	csvFile="$txtFile.csv"
	echo "generation csv file from $txtFile"
	$SCRIPT_LOCATION/conv_csv --file=$txtFile --csv=$csvFile
	rm $txtFile

	#mv $csvFile $TARGET_DIR
done

#rm $TMP_DIR -r
