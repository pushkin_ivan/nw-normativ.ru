package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
)

// Columns
const (
	PNDF_REGISTRY_NUMBER = iota
	PNDF_NAME_AND_CODE
	PNDF_SCALE_RANGE
	PNDF_EXPERIENCE
	PNDF_AUTHOR_ID
	PNDF_NOTICES
	REGISTRY_COLUMN_COUNT
)

func main() {
	log.Println("starting pnd f convertor")

	var fileName string
	flag.StringVar(&fileName, "file", "", "text file of pnd f list")

	var csvFileName string
	flag.StringVar(&csvFileName, "csv", "", "csv file name")

	flag.Parse()

	if len(fileName) == 0 {
		log.Fatalln("passed empty file name")
	}
	if len(csvFileName) == 0 {
		log.Fatalln("passed empty csv file name")
	}

	buff, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatalln("cannot read file:", err)
	}

	csvFile, err := os.Create(csvFileName)
	if err != nil {
		log.Fatalln("cannot write csv file:", err)
	}
	defer csvFile.Close()

	fmt.Fprintln(csvFile, "code;name;description")

	// Parse pnd f registry file
	chrows := ExtractRows(buff)
	for row := range chrows {
		if len(row) != REGISTRY_COLUMN_COUNT {
			continue
		}

		var code, name, description []byte
		code, name = SplitCodeName(row[PNDF_NAME_AND_CODE])
		description = MakeDescription(row[PNDF_SCALE_RANGE], row[PNDF_EXPERIENCE])

		fmt.Fprintf(csvFile, "%q;%q;\"%s\"\n", code, name, description)
	}

	log.Println("goodbie")
}

var rowExp = regexp.MustCompile("\n\\|\\d+\\s*\\|")

func ExtractRows(buff []byte) <-chan [][]byte {

	output := make(chan [][]byte)

	go func() {
		defer close(output)

		indexList := rowExp.FindAllIndex(buff, -1)

		buffLen := len(buff)
		indexList = append(indexList, []int{buffLen, buffLen})

		var start = -1
		for i := 0; i < len(indexList); i++ {
			if start > 0 {
				end := indexList[i][0]

				rowBuff := buff[start:end]
				rowBuff = bytes.TrimSpace(rowBuff)

				lines := bytes.Split(rowBuff, []byte{'\n'})

				var (
					columns     [][]byte
					columnCount = -1 // = len(columns)
				)

				for _, line := range lines {
					line = bytes.TrimSpace(line)
					if len(line) == 0 {
						break
					}
					line = bytes.Trim(line, "|")
					if bytes.Count(line, []byte{'|'}) == 0 {
						break
					}

					tails := bytes.SplitN(line, []byte{'|'}, columnCount)
					if columnCount > 0 && len(tails) != columnCount {
						break
					}

					for j := 0; j < len(tails); j++ {
						tails[j] = bytes.TrimSpace(tails[j])
					}

					if columnCount == -1 {
						// Init row
						columns = make([][]byte, len(tails))
						for j := 0; j < len(tails); j++ {
							if len(tails[j]) > 0 {
								columns[j] = make([]byte, len(tails[j]))
								copy(columns[j], tails[j])
							} else {
								columns[j] = make([]byte, 0)
							}
						}

						columnCount = len(tails)
					} else {
						// Append tails
						for j := 0; j < columnCount; j++ {
							if len(tails[j]) == 0 {
								continue
							}

							columns[j] = append(columns[j], ' ')
							columns[j] = append(columns[j], tails[j]...)
						}
					}
				}

				if columnCount > 0 {
					output <- columns
				}
			}

			start = indexList[i][0]
		}
	}()

	return output
}

func ReplaceQuotes(s []byte) []byte {
	// Replace 0xab
	s = bytes.Replace(s, []byte{0xc2, 0xab}, []byte{'"'}, -1)
	// Replace 0x2039
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0xb9}, []byte{'"'}, -1)
	// Replace 0xbb
	s = bytes.Replace(s, []byte{0xc2, 0xbb}, []byte{'"'}, -1)
	// Replace 0x203a
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0xba}, []byte{'"'}, -1)
	// Replace 0x201e
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9e}, []byte{'"'}, -1)
	// Replace 0x201a
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9a}, []byte{'"'}, -1)
	// Replace 0x201c
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9c}, []byte{'"'}, -1)
	// Replace 0x201f
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9f}, []byte{'"'}, -1)
	// Replace 0x2018
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x98}, []byte{'"'}, -1)
	// Replace 0x201b
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9b}, []byte{'"'}, -1)
	// Replace 0x201d
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x9d}, []byte{'"'}, -1)
	// Replace 0x2019
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x99}, []byte{'"'}, -1)

	return s
}

func ReplaceDashes(s []byte) []byte {
	// Replace 0x2012
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x92}, []byte{'-'}, -1)
	// Replace 0x2013
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x93}, []byte{'-'}, -1)
	// Replace 0x2014
	s = bytes.Replace(s, []byte{0xe2, 0x80, 0x94}, []byte{'-'}, -1)

	return s
}

func ReplaceHorizontalEllipsis(s []byte) []byte {
	// Replace 0x2026
	return bytes.Replace(s, []byte{0xe2, 0x80, 0xa6}, []byte("..."), -1)
}

func ReplaceIntellectualPropertySymbols(s []byte) []byte {
	// Replace 0x00a9
	s = bytes.Replace(s, []byte{0xc2, 0xa9}, []byte("(c)"), -1)
	// Replace 0x00ae
	s = bytes.Replace(s, []byte{0xc2, 0xae}, []byte("(r)"), -1)
	// Replace 0x2117
	s = bytes.Replace(s, []byte{0xe2, 0x84, 0x97}, []byte("(p)"), -1)
	// Replace 0x2122
	s = bytes.Replace(s, []byte{0xe2, 0x84, 0xa2}, []byte("(tm)"), -1)

	return s
}

var codeExp = regexp.MustCompile(`ПНД ?Ф ?((:?СБ|Т ?)?[0-9\.:-]+\d)`)

const CODE_PREFIX = "ПНД Ф "

var codePrefix = []byte(CODE_PREFIX)

func SplitCodeName(s []byte) (code, name []byte) {
	if p := codeExp.FindSubmatch(s); len(p) > 0 {
		code = append(codePrefix, p[1]...)
	}

	name = bytes.SplitN(s, []byte("ПНД"), 2)[0]
	name = ReplaceQuotes(name)
	name = ReplaceDashes(name)
	name = ReplaceHorizontalEllipsis(name)
	name = ReplaceIntellectualPropertySymbols(name)
	name = bytes.TrimSpace(name)

	return
}

var topicScaleRange = []byte("Диапазон измерений:")

func MakeDescription(scaleRange, experience []byte) (desc []byte) {
	if len(scaleRange) > 0 {
		desc = append(topicScaleRange, '\n')
		desc = append(desc, scaleRange...)
		desc = append(desc, '\n')
	}

	if len(experience) > 0 {
		desc = append(desc, experience...)
	}

	return
}
