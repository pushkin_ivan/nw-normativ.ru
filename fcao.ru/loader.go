package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"

	"github.com/PuerkitoBio/goquery"
)

// Find all links for doc files
const SELECT_DOC_LINKS = `a[href$="doc"]`

func main() {
	log.Println("starting pnd f loader")

	// Setup and parse parameters

	var registryUrl string
	flag.StringVar(&registryUrl, "url", "", "pnd f registry url")

	var targetDir string
	flag.StringVar(&targetDir, "dir", "", "target directory")

	flag.Parse()

	// Checking registry url

	if len(registryUrl) == 0 {
		log.Fatalln("passed empty registry url")
	}
	registryUrlObj, err := url.Parse(registryUrl)
	if err != err {
		log.Fatalln("passed invalid url")
	}
	if !registryUrlObj.IsAbs() {
		log.Fatalln("url must be absolute")
	}

	// Load and parse page

	doc, err := goquery.NewDocument(registryUrl)
	if err != nil {
		log.Fatalln("cannot get document from url:", err)
	}

	docLinks := doc.Find(SELECT_DOC_LINKS)
	if docLinks.Length() == 0 {
		log.Fatalln("cannot find any link for doc document")
	}

	// Parse and rewrite found links

	docLinks.Each(func(i int, s *goquery.Selection) {
		if docUrlHref, exists := s.Attr("href"); exists {
			log.Println("found:", docUrlHref)

			docUrlHrefObj, err := url.Parse(docUrlHref)
			if err != nil {
				log.Println("cannot parse document url:", err)
			} else {
				if !docUrlHrefObj.IsAbs() {
					docUrlHrefObj.Scheme = registryUrlObj.Scheme
					docUrlHrefObj.Opaque = registryUrlObj.Opaque
					docUrlHrefObj.User = registryUrlObj.User
					docUrlHrefObj.Host = registryUrlObj.Host
				}

				err := loadFile(docUrlHrefObj, targetDir)
				if err != nil {
					log.Println(err)
				}
			}
		}
	})

	log.Println("goodbie")
}

func loadFile(fileUrl *url.URL, targetDir string) error {
	resp, err := http.Get(fileUrl.String())
	if err != nil {
		return fmt.Errorf("cannot load file: %s", err)
	}
	defer resp.Body.Close()

	_, fileName := path.Split(fileUrl.Path)

	f, err := os.Create(path.Join(targetDir, fileName))
	if err != nil {
		return fmt.Errorf("cannot write file: %s", err)
	}
	defer f.Close()

	size, err := io.Copy(f, resp.Body)
	if err != nil {
		return fmt.Errorf("cannot load or write file: %s", err)
	}

	log.Printf("loaded file: %q. size: %d bytes\n", fileName, size)

	return nil
}
