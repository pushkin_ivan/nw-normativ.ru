package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"syscall"
	"time"

	"github.com/PuerkitoBio/goquery"
)

const (
	CHAN_REQUESTS_BUFFER_SIZE  = 10
	CHAN_BODIES_BUFFER_SIZE    = 10
	CHAN_DOCUMENTS_BUFFER_SIZE = 10
	CHAN_ERRORS_BUFFER_SIZE    = 5
)

const (
	SELECTOR_DOCUMENT_ROW = "table[toplevel] tbody tr:not([valign], [class], [style])"
)

// http://www.fundmetrology.ru/06_metod/2list.aspx
var rawRequest = []byte(`POST /06_metod/2list.aspx HTTP/1.0
Host: www.fundmetrology.ru
Content-Length: CONTENT_LENGTH_HERE
Origin: http://www.fundmetrology.ru
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Referer: http://www.fundmetrology.ru/06_metod/2list.aspx

MSOWebPartPage_PostbackSource=&MSOTlPn_SelectedWpId=&MSOTlPn_View=0&MSOTlPn_ShowSettings=False&MSOGallery_SelectedLibrary=&MSOGallery_FilterString=&MSOTlPn_Button=none&__EVENTTARGET=g_cc758123_145d_4f18_8bd8_f53cba0906ac2&__EVENTARGUMENT=TARGET_PAGE_NUMBER&__REQUESTDIGEST=0x04BAEB77A6DD38175DD84283BBE951A44E8435FF3CF861370AA3DC7B3524255FF0D16262664DF80EAC39E35504E2B1D3EC9EDF070CDE9B64D24382187CFB53D9%2C05+Jul+2016+14%3A37%3A01+-0000&MSOSPWebPartManager_DisplayModeName=Browse&MSOWebPartPage_Shared=&MSOLayout_LayoutChanges=&MSOLayout_InDesignMode=&MSOSPWebPartManager_OldDisplayModeName=Browse&MSOSPWebPartManager_StartWebPartEditingName=false&__VIEWSTATE=%2FwEPDwUBMA9kFgJmD2QWAgIDD2QWBgIDDw8WEB4LRGVzY3JpcHRpb25lHglEaXJlY3Rpb24LKipTeXN0ZW0uV2ViLlVJLldlYkNvbnRyb2xzLkNvbnRlbnREaXJlY3Rpb24AHgVUaXRsZQUG0KLQodCYHgpDaHJvbWVUeXBlAgIeBF8hU0ICgIMIHgVXaWR0aBweBkhlaWdodBweC1BhcmFtVmFsdWVzMuUFAAEAAAD%2F%2F%2F%2F%2FAQAAAAAAAAAMAgAAAFhNaWNyb3NvZnQuU2hhcmVQb2ludCwgVmVyc2lvbj0xMi4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj03MWU5YmNlMTExZTk0MjljBQEAAAA9TWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViUGFydFBhZ2VzLlBhcmFtZXRlck5hbWVWYWx1ZUhhc2h0YWJsZQEAAAAFX2NvbGwDHFN5c3RlbS5Db2xsZWN0aW9ucy5IYXNodGFibGUCAAAACQMAAAAEAwAAABxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlBwAAAApMb2FkRmFjdG9yB1ZlcnNpb24IQ29tcGFyZXIQSGFzaENvZGVQcm92aWRlcghIYXNoU2l6ZQRLZXlzBlZhbHVlcwAAAwMABQULCBxTeXN0ZW0uQ29sbGVjdGlvbnMuSUNvbXBhcmVyJFN5c3RlbS5Db2xsZWN0aW9ucy5JSGFzaENvZGVQcm92aWRlcgjsUTg%2F3gQAAAoKLwAAAAkEAAAACQUAAAAQBAAAABAAAAAGBgAAAANwdzQGBwAAAANwdzUGCAAAAANwdzcGCQAAAARwbnVtBgoAAAAFVG9kYXkGCwAAAANwcnIGDAAAAARwc3RyBg0AAAAMZHZ0X2ZpcnN0cm93Bg4AAAAEcG5hbQYPAAAABlVzZXJJRAYQAAAAA3B6egYRAAAAA3B3MgYSAAAAA3B3MwYTAAAAA3B3MQYUAAAAA3B3NgYVAAAAA3BubhAFAAAAEAAAAAYWAAAAAAkWAAAACRYAAAAGFwAAAAEtBhgAAAAUMjAxNi0wNy0wNVQxNzozNzowMloJFgAAAAYaAAAAAS4GGwAAAAUyMjQwMQYcAAAAAS4GHQAAAA9DdXJyZW50VXNlck5hbWUJFgAAAAkWAAAACRYAAAAJFgAAAAkWAAAACRYAAAALZBYCZg8PZA8QFgpmAgECAgIDAgQCBQIGAgcCCAIJFgoWAh4OUGFyYW1ldGVyVmFsdWVlFgIfCGUWAh8IZRYCHwhlFgIfCGUWAh8IZRYCHwhlFgIfCGUWAh8IZRYCHwhlDxYKAgcCBwIHAgcCBwIHAgcCBwIHAgcWAQWNAU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYlBhcnRQYWdlcy5EYXRhRm9ybVBhcmFtZXRlciwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5Y2RkAgUPDxYQHwBlHwELKwQAHwIFC04tbWV0b2QgKDEpHwMCAh8EAoCDCB8FHB8GGwAAAAAAwHJAAQAAAB8HMpoEAAEAAAD%2F%2F%2F%2F%2FAQAAAAAAAAAMAgAAAFhNaWNyb3NvZnQuU2hhcmVQb2ludCwgVmVyc2lvbj0xMi4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj03MWU5YmNlMTExZTk0MjljBQEAAAA9TWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViUGFydFBhZ2VzLlBhcmFtZXRlck5hbWVWYWx1ZUhhc2h0YWJsZQEAAAAFX2NvbGwDHFN5c3RlbS5Db2xsZWN0aW9ucy5IYXNodGFibGUCAAAACQMAAAAEAwAAABxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlBwAAAApMb2FkRmFjdG9yB1ZlcnNpb24IQ29tcGFyZXIQSGFzaENvZGVQcm92aWRlcghIYXNoU2l6ZQRLZXlzBlZhbHVlcwAAAwMABQULCBxTeXN0ZW0uQ29sbGVjdGlvbnMuSUNvbXBhcmVyJFN5c3RlbS5Db2xsZWN0aW9ucy5JSGFzaENvZGVQcm92aWRlcgjsUTg%2FGgAAAAoKCwAAAAkEAAAACQUAAAAQBAAAAAMAAAAGBgAAAAZVc2VySUQGBwAAAAVUb2RheQYIAAAABlBhcmFtMRAFAAAAAwAAAAYJAAAAD0N1cnJlbnRVc2VyTmFtZQYKAAAAFDIwMTYtMDctMDVUMTc6Mzc6MDJaBgsAAAAAC2RkAgcPDxYQHwBlHwELKwQAHwIFBtCi0KHQmB8DAgIfBAKAgwgfBRwfBhwfBzLIBQABAAAA%2F%2F%2F%2F%2FwEAAAAAAAAADAIAAABYTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwUBAAAAPU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYlBhcnRQYWdlcy5QYXJhbWV0ZXJOYW1lVmFsdWVIYXNodGFibGUBAAAABV9jb2xsAxxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlAgAAAAkDAAAABAMAAAAcU3lzdGVtLkNvbGxlY3Rpb25zLkhhc2h0YWJsZQcAAAAKTG9hZEZhY3RvcgdWZXJzaW9uCENvbXBhcmVyEEhhc2hDb2RlUHJvdmlkZXIISGFzaFNpemUES2V5cwZWYWx1ZXMAAAMDAAUFCwgcU3lzdGVtLkNvbGxlY3Rpb25zLklDb21wYXJlciRTeXN0ZW0uQ29sbGVjdGlvbnMuSUhhc2hDb2RlUHJvdmlkZXII7FE4P%2F0BAAAKChcAAAAJBAAAAAkFAAAAEAQAAAAPAAAABgYAAAAEcHN0cgYHAAAABVRvZGF5BggAAAADcHcyBgkAAAADcHczBgoAAAAEcG51bQYLAAAAA3B3MQYMAAAAA3B3NgYNAAAAA3B3NwYOAAAAA3B3NAYPAAAAA3B3NQYQAAAAA3B6egYRAAAAA3BycgYSAAAABHBuYW0GEwAAAANwbm4GFAAAAAZVc2VySUQQBQAAAA8AAAAGFQAAAAEuBhYAAAAUMjAxNi0wNy0wNVQxNzozNzowMloGFwAAAAAJFwAAAAYYAAAAAS0JFwAAAAkXAAAACRcAAAAJFwAAAAkXAAAACRcAAAAJFwAAAAYaAAAAAS4JFwAAAAYcAAAAD0N1cnJlbnRVc2VyTmFtZQtkFgJmDw9kDxAWCmYCAQICAgMCBAIFAgYCBwIIAgkWChYCHwhlFgIfCGUWAh8IZRYCHwhlFgIfCGUWAh8IZRYCHwhlFgIfCGUWAh8IZRYCHwhlDxYKAgcCBwIHAgcCBwIHAgcCBwIHAgcWAQWNAU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYlBhcnRQYWdlcy5EYXRhRm9ybVBhcmFtZXRlciwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5Y2RkZNX6Us%2BYoEK9hF8dY2oBaZkkNYRY&Text2=&Text1=&Text3=&Text2=`)

const ADDR = "85.143.125.59:80"

var duration time.Duration

func init() {
	flag.DurationVar(&duration, "duration", time.Hour*24, "execution duration")
}

func numOfDigs(d int) int {
	var number_of_digits int
	if d == 0 {
		return 1
	}
	for d > 0 {
		number_of_digits++
		d /= 10
	}
	return number_of_digits
}

func genRequest(cancel <-chan struct{}, from, to int) <-chan []byte {
	rawRequestChan := make(chan []byte, CHAN_REQUESTS_BUFFER_SIZE)

	go func() {
		defer close(rawRequestChan)

		for i := from; i < to; i++ {
			rawReq := bytes.Replace(rawRequest, []byte("TARGET_PAGE_NUMBER"), []byte(fmt.Sprintf(`dvt_firstrow%%3D%%7B%d%%7D`, i*100+1)), 1)
			rawReq = bytes.Replace(rawReq, []byte("CONTENT_LENGTH_HERE"),
				[]byte(fmt.Sprintf(`%d`, 4483+numOfDigs(i*100+1))), 1)
			log.Printf("Progress: %d/%d\r", i, to-1)
			select {
			case rawRequestChan <- rawReq:
			case <-cancel:
				return
			}
		}
	}()

	return rawRequestChan
}

type Document struct {
	code, name, description string
}

var wsExpr = regexp.MustCompile(`\s+`)

func genDocs(cancel <-chan struct{}, rawRequestChan <-chan []byte) (<-chan *Document, <-chan error) {
	var (
		docChan = make(chan *Document, CHAN_DOCUMENTS_BUFFER_SIZE)
		errChan = make(chan error, CHAN_ERRORS_BUFFER_SIZE)
	)

	go func() {
		defer func() {
			close(docChan)
			close(errChan)
		}()

		for rawReq := range rawRequestChan {

			conn, err := net.Dial("tcp", ADDR)
			if err != nil {
				log.Println(err)
				continue
			}

			if _, err = conn.Write(rawReq); err != nil {
				log.Println(err)
				continue
			}

			resp, err := http.ReadResponse(bufio.NewReader(conn), nil)
			if err != nil {
				log.Println(err)
				continue
			}

			// log.Println(resp)

			if doc, err := goquery.NewDocumentFromReader(resp.Body); err == nil {
				doc.Find(SELECTOR_DOCUMENT_ROW).Each(func(i int, s *goquery.Selection) {
					if s.Children().Length() == 4 {
						code := s.Find("td:nth-child(1)").Text()
						name := s.Find("td:nth-child(2)").Text()
						desc := fmt.Sprintf("Свидетельство №%s от %s",
							s.Find("td:nth-child(3)").Text(),
							s.Find("td:nth-child(4)").Text())

						code = wsExpr.ReplaceAllLiteralString(code, " ")
						name = wsExpr.ReplaceAllLiteralString(name, " ")
						desc = wsExpr.ReplaceAllLiteralString(desc, " ")

						select {
						case docChan <- &Document{code, name, desc}:
						case <-cancel:
							return
						}
					}
				})
			} else {
				select {
				case errChan <- err:
				case <-cancel:
					return
				}
			}

			conn.Close()
		}
	}()

	return docChan, errChan
}

func main() {
	flag.Parse()

	log.SetFlags(log.LstdFlags)
	log.SetOutput(os.Stderr)

	log.Println("starting parser")

	// Creating global stopper
	var (
		sigChan         = make(chan os.Signal, 1)
		globCancel      = make(chan struct{})
		execTimeoutChan = time.After(duration)
	)

	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	defer close(sigChan)

	go func() {
		select {
		case <-sigChan:
			log.Println("program interrupted")
		case <-execTimeoutChan:
			log.Println("execution time is out")
		}

		close(globCancel)
	}()

	// Starting

	var (
		requestChan      = genRequest(globCancel, 0, 225)
		docChan, errChan = genDocs(globCancel, requestChan)
	)

	fmt.Println("code;name;description")
	for doc := range docChan {
		fmt.Fprintf(os.Stdout, `"%s";"%s";"%s"`+"\n", doc.code, doc.name, doc.description)
	}

	for err := range errChan {
		log.Println("error:", err)
	}
}
