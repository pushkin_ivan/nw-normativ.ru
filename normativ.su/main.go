// normativ.su project main.go
package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	LINK_FORMAT = "http://www.normativ.su/catalog/standart/1004/?PAGEN_1=%d"
	TABLE       = `table[id="table"]`
	ROW         = `tbody > tr[valign="top"]`
	PRICE       = "strong > s"
)

func link(page int) string {
	if page > 0 {
		return fmt.Sprintf(LINK_FORMAT, page)
	}
	return ""
}

func main() {
	var i = 1
	var first_article, first_name string
	f, err := os.Create("result.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	f.Write([]byte(
		`"Artikul";"Naimenovanie";"tsena electronnoy versii"; "tsena pechatnoy versii"\n`))
	for {
		time.Sleep(time.Millisecond * 40)
		if resp, err := http.Get(link(i)); err == nil {
			if resp.StatusCode != http.StatusOK {
				resp.Body.Close()
				continue
			}
			if doc, err := goquery.NewDocumentFromResponse(resp); err == nil {
				doc.Find(TABLE).Last().Find(ROW).Each(func(j int, s *goquery.Selection) {
					article := s.Find("td").Eq(0).Find("a").Text()
					name := s.Find("td").Eq(1).Text()
					first_price := s.Find("td").Eq(3).Find(PRICE).Text()
					second_price := s.Find("td").Eq(4).Find(PRICE).Text()
					fmt.Fprintf(f, `"%s";"%s";"%s";"%s"`+"\n",
						article, name, first_price, second_price)
					fmt.Printf("doc[%26s], page[%4d]\r", article, i)
					if i == 1 && j == 0 {
						first_article, first_name = article, name
					} else if j == 0 && first_article == article && first_name == name {
						os.Exit(0)
					}
				})
			} else {
				log.Println(err)
				continue
			}
			resp.Body.Close()
			i++
		} else {
			log.Println(err)
		}
	}
}
