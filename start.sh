#!/bin/bash

echo "wellcome to nwn parser!"

CWD=$(pwd)
TARGET_DIR="$CWD/result"
SCRIPT_LOCATION=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ -d "$TARGET_DIR" ]; then
	rm $TARGET_DIR/* -r
else
	mkdir $TARGET_DIR
fi

# fcao.ru
echo "loading standards from fcao.ru"
$SCRIPT_LOCATION/fcao.ru/pnd_f.sh $TARGET_DIR

# snti.ru
echo "loading standards from snti.ru"
$SCRIPT_LOCATION/snti.ru/SanPin_MUK_MU_MP_GN_SP_SN_R.sh $TARGET_DIR
$SCRIPT_LOCATION/snti.ru/rd52.sh $TARGET_DIR

# www.standards.ru
echo "loading standards from www.standards.ru"
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "belgis" 60 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "gost" 60 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "iso" 60 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "mec" 60 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "OK1" 2 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "ost" 60 $TARGET_DIR false
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "rst" 30 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "STRK" 60 $TARGET_DIR true
$SCRIPT_LOCATION/www.standards.ru/load_standards.sh "tu" 60 $TARGET_DIR false
