#!/bin/bash

echo "loading snips, rd52"

TARGET_DIR=$1
FILE_NAME="/tmp/rd52.csv"
SCRIPT_LOCATION=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "code;name;description" > $FILE_NAME

$SCRIPT_LOCATION/snti_parser --url="http://www.snti.ru/snips_rd52.htm" 1>> $FILE_NAME

if [ ! -z "$TARGET_DIR" ]; then
	if [ -d "$TARGET_DIR" ]; then
		mv $FILE_NAME $TARGET_DIR
	fi
fi
