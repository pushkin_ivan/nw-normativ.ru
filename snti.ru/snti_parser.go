package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html/charset"
)

const (
	CHARSET = "windows-1251"

	S_DATA = `table[cellspacing="2"][cellpadding="4"] p`

	EXP = `([А-Я][а-яА-Я]{0,5} [0-9\.\/аб-]+) (.+)`

	DOC_CODE = 1
	DOC_NAME = 2

	CSV_ROW_FORMAT = `"%s";"%s";` + "\n"
)

var rexp = regexp.MustCompile(EXP)

func main() {
	var url string
	flag.StringVar(&url, "url", "", "url")
	flag.Parse()

	if len(url) == 0 {
		log.Fatalln("empty url")
	}

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln("cannot load data:", err)
	}

	if resp.StatusCode != http.StatusOK {
		log.Fatalln("received unsuccessful status code")
	}

	r, err := charset.NewReader(resp.Body, CHARSET)
	if err != nil {
		log.Fatalln("cannot convert charset:", err)
	}

	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatalln("cannot parse data:", err)
	}

	dataNode := doc.Find(S_DATA)
	if dataNode.Size() == 0 {
		log.Fatalln("data was not found")
	}

	rows := strings.Split(dataNode.Text(), "\n")

	for _, row := range rows {
		row = strings.TrimSpace(row)
		if len(row) > 0 {
			row = strings.TrimLeft(row, "+")

			doc := rexp.FindStringSubmatch(row)
			if len(doc) == 3 {
				fmt.Printf(CSV_ROW_FORMAT,
					addDoubleQuotes(doc[DOC_CODE]),
					addDoubleQuotes(doc[DOC_NAME]),
				)
			}
		}
	}
}

func addDoubleQuotes(s string) string {
	return strings.Replace(s, `"`, `""`, -1)
}
