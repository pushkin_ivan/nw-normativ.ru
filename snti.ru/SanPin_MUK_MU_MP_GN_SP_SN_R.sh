#!/bin/bash

echo "loading SanPin, MUK, MU, MP, GN, SP, SN, R"

TARGET_DIR=$1
FILE_NAME="/tmp/SanPin_MUK_MU_MP_GN_SP_SN_R.csv"
SCRIPT_LOCATION=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "code;name;description" > $FILE_NAME

urls=()
urls+=("http://www.snti.ru/snips_sanpin-0.htm")
urls+=("http://www.snti.ru/snips_sanpin-1.htm")
urls+=("http://www.snti.ru/snips_sanpin-2.htm")
urls+=("http://www.snti.ru/snips_sanpin-3.htm")
urls+=("http://www.snti.ru/snips_sanpin-4.htm")
urls+=("http://www.snti.ru/snips_sanpin-5.htm")
urls+=("http://www.snti.ru/snips_sanpin-6.htm")
urls+=("http://www.snti.ru/snips_sanpin-7.htm")
urls+=("http://www.snti.ru/snips_sanpin-8.htm")
urls+=("http://www.snti.ru/snips_sanpin-9.htm")
urls+=("http://www.snti.ru/snips_sanpin1.htm")
urls+=("http://www.snti.ru/snips_sanpin2.htm")

for url in "${urls[@]}"; do
	$SCRIPT_LOCATION/snti_parser --url=$url 1>> $FILE_NAME
done

if [ ! -z "$TARGET_DIR" ]; then
	if [ -d "$TARGET_DIR" ]; then
		mv $FILE_NAME $TARGET_DIR
	fi
fi
