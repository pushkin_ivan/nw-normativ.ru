package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html/charset"
)

func urlGen(done <-chan struct{}, format string, start, finish int,
) <-chan string {

	var urlch = make(chan string)

	go func() {
		log.Println("starting url generator")
		defer func() {
			log.Println("finishing url generator")
			close(urlch)
		}()

		for i := start; i <= finish; i++ {
			select {
			case urlch <- fmt.Sprintf(format, i):
			case <-done:
				return
			}
		}
	}()

	return urlch
}

type errUrlLoading struct {
	err error
	url string
}

func (e *errUrlLoading) Error() string {
	return fmt.Sprintf("loading url [%s] error: %s", e.url, e.err)
}

func readerGen(done <-chan struct{}, urlch <-chan string, alim int,
) <-chan io.Reader {

	var rch = make(chan io.Reader)

	go func() {
		log.Println("starting reader generator")
		defer func() {
			log.Println("finishing reader generator")
			close(rch)
		}()

		for url := range urlch {
			log.Println("url:", url)

			select {
			case <-done:
				return
			default:
			}

			var acount int // Attempt count

		retrying:

			resp, err := http.Get(url)
			if err != nil {
				log.Println(&errUrlLoading{err, url})
				continue
			}

			if resp.StatusCode != http.StatusOK {
				log.Println("status code:", resp.StatusCode)

				acount++

				if alim > acount {
					log.Println("retry to load url:", url)
					goto retrying
				}

				log.Println(&errUrlLoading{
					errors.New("retrying limit was reached"),
					url,
				})

				continue
			}

			rch <- resp.Body
		}

	}()

	return rch
}

func merge(done <-chan struct{}, rchs ...<-chan io.Reader,
) <-chan io.Reader {
	var (
		wg  sync.WaitGroup
		out = make(chan io.Reader)
	)

	output := func(rch <-chan io.Reader) {
		defer wg.Done()

		for r := range rch {
			select {
			case out <- r:
			case <-done:
				return
			}
		}
	}

	wg.Add(len(rchs))

	for _, rch := range rchs {
		go output(rch)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

type Doc struct {
	code, name string
}

func docGen(done <-chan struct{}, rch <-chan io.Reader,
	withPrice bool) <-chan *Doc {
	var dch = make(chan *Doc)

	go func() {
		log.Println("starting document generator")
		defer func() {
			log.Println("finishing document generator")
			close(dch)
		}()

		for r := range rch {
			select {
			case <-done:
				return
			default:
			}

			docs, err := readDocuments(r, withPrice)
			if err != nil {
				log.Println(err)
				continue
			}

			for _, doc := range docs {
				dch <- doc
			}
		}
	}()

	return dch
}

type errCannotReadDocs struct {
	err error
}

func (e *errCannotReadDocs) Error() string {
	return "cannot read documents: " + e.err.Error()
}

const (
	CHARSET = "windows-1251"

	// Selectors:

	// Row
	S_DOC_ROW = `table[class="typetable"]>tbody>tr[valign="top"]`

	// Code. Select from row
	S_DOC_CODE = `td[class="tx12"]>div[align="left"]>a`

	// Name. Select from second child of document row :eq(1)
	S_DOC_NAME = `div`

	// Price. Select from row
	S_DOC_PRICE = `td>table span.num`
)

func readDocuments(r io.Reader, withPrice bool) ([]*Doc, error) {
	r, err := charset.NewReader(r, CHARSET)
	if err != nil {
		return nil, &errCannotReadDocs{err}
	}

	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		return nil, &errCannotReadDocs{err}
	}

	rows := doc.Find(S_DOC_ROW)
	if rows.Size() == 0 {
		return nil, &errCannotReadDocs{
			errors.New("cannot find documents"),
		}
	}

	docs := make([]*Doc, 0, rows.Size())

	doc.Find(S_DOC_ROW).Each(func(i int, s *goquery.Selection) {
		if !withPrice || s.Find(S_DOC_PRICE).Size() == 1 {
			docs = append(docs, &Doc{
				strings.TrimSpace(s.Find(S_DOC_CODE).Text()),
				strings.TrimSpace(s.Children().Eq(1).
					Find(S_DOC_NAME).Text()),
			})
		}
	})

	return docs, nil
}

func addDoubleQuotes(s string) string {
	return strings.Replace(s, `"`, `""`, -1)
}

const DOC_CSV_ROW_FORMAT = `"%s";"%s";` + "\n"

func docWriter(done <-chan struct{}, w io.Writer, dch <-chan *Doc,
) error {

	log.Println("starting document writer")
	defer log.Println("finishing document writer")

	fmt.Fprintln(w, "code;name;description")

	for d := range dch {
		select {
		case <-done:
			return nil
		default:
			_, err := fmt.Fprintf(
				w,
				DOC_CSV_ROW_FORMAT,
				addDoubleQuotes(d.code),
				addDoubleQuotes(d.name),
			)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func main() {
	log.Println("starting program")

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	log.Println("parsing parameters")

	var goroutinesCount, retryingLimit int
	flag.IntVar(&goroutinesCount, "goroutines_count", 1,
		"goroutines count")
	flag.IntVar(&retryingLimit, "retrying_limit", 15,
		"retrying limit")

	var timeout time.Duration
	flag.DurationVar(&timeout, "timeout", 0, "working timeout")

	var format string
	flag.StringVar(&format, "format", "", "")

	var start, finish int
	flag.IntVar(&start, "start", 0, "first index")
	flag.IntVar(&finish, "finish", 0, "last index")

	var outputFile string
	flag.StringVar(&outputFile, "output_file", "output.csv",
		"output file")

	var withPrice bool
	flag.BoolVar(&withPrice, "save_with_price", true,
		"save documents with price")

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	flag.Parse()

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	if goroutinesCount < 1 {
		log.Fatalln("invalid goroutines count")
	}
	if retryingLimit < 0 {
		log.Fatalln("invalid retrying limit")
	}
	if len(format) == 0 {
		log.Fatalln("empty format")
	}
	if strings.Count(format, "%d") != 1 {
		log.Fatalln(`format must contain one integer specifier "%d"`)
	}
	if start > finish {
		log.Fatalln("start index cannot be more than finish index")
	}
	if len(outputFile) == 0 {
		log.Fatalln("empty output file name")
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	runtime.GOMAXPROCS(runtime.NumCPU())

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	output, err := os.Create(outputFile)
	if err != nil {
		log.Fatalln(err)
	}
	defer output.Close()

	done := make(chan struct{})
	if timeout > 0 {
		go func() {
			select {
			case <-time.After(timeout):
				close(done)
			case <-done:
			}
		}()
	} else {
		defer close(done)
	}

	urlch := urlGen(done, format, start, finish)

	rchs := make([]<-chan io.Reader, 0, goroutinesCount)
	for i := 0; i < goroutinesCount; i++ {
		rchs = append(rchs, readerGen(done, urlch, retryingLimit))
	}

	dch := docGen(done, merge(done, rchs...), withPrice)

	if err := docWriter(done, output, dch); err != nil {
		log.Fatalln(err)
	}

	log.Println("goodbye")
}
