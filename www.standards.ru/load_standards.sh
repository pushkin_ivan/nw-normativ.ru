#!/bin/bash

if [ "$#" -eq 5 ]; then
    echo "illegal number of parameters"
    exit
fi

catalog_id=$1
g_count=$2
target_directory=$3
save_with_price=$4

directory_url="http://www.standards.ru/catalog/$catalog_id.aspx"
echo "loading standards from $directory_url"
if [ -e $target_directory ]; then
	echo "save data to $target_directory"
fi
echo "use $g_count goroutines"

script_location=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# printf formatted url
format="http://www.standards.ru/doc.aspx?control=&search=&sort=%%20ASC&catalogid=$catalog_id&classid=-1&s=-1&page=%d"

# pages
start=0
finish=$($script_location/get_page_count --url="$directory_url")
echo "page range: start=$start; finish=$finish"

# output file name
output="/tmp/$catalog_id.csv"
echo "output file: $output"

if [ -f $output ]; then
	rm $output
fi

echo "start"
$script_location/standards_parser --format=$format --start=$start \
--finish=$finish --output_file=$output --goroutines_count=$g_count \
--save_with_price=$save_with_price

if [ ! -z "$target_directory" ]; then
	if [ -d "$target_directory" ]; then
		echo "move $output to $target_directory"
		mv $output $target_directory
	fi
fi
