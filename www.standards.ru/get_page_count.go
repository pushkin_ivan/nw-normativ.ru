package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	var pageUrl string
	flag.StringVar(&pageUrl, "url", "", "page url")

	flag.Parse()

	if URL, err := url.Parse(pageUrl); err != nil {
		log.Fatalln("invalid page url:", err)
	} else if !URL.IsAbs() {
		log.Fatalln("page url must be absolute")
	}

	doc, err := goquery.NewDocument(pageUrl)
	if err != nil {
		log.Fatalln("cannot get document")
	}

	pageLink := doc.Find(`a[href^="/doc.aspx"]`).Last()
	if pageLink.Length() == 0 {
		log.Fatalln("cannot find any page link")
	}

	if query, exists := pageLink.Attr("href"); exists {
		if v, err := url.ParseQuery(query); err != nil {
			log.Fatalln("invalid link query")
		} else if page := v.Get("page"); len(page) == 0 {
			log.Fatalln("query not contains page id")
		} else {
			fmt.Print(page)
		}
	} else {
		log.Fatalln("found link is invalid")
	}
}
